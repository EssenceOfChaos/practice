defmodule Practice.Stack do
  @moduledoc """
  An implementation of the Stack data structure with lists.
  """
  defstruct size: 0, items: []

  @doc """
  Returns a new stack.
  ## Example
      iex> stack = Practice.Stack.new()
      %Practice.Stack{size: 0, stack: []}
  """

  def new(), do: %Practice.Stack{}

  @doc """
   Return the stack with the given element pushed into it
  """

  def push(stack = %Practice.Stack{size: s, items: list}, value) do
    %{stack | size: s + 1, items: [value | list]}
  end

  @doc """
  Returns the size of the stack.
  """

  def size(%Practice.Stack{size: s}), do: s
end
