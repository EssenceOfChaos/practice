defmodule Practice.BST do
  @moduledoc """
  Define a BST and relative interview questions regarding Binary Search Trees
  """
  defmodule Node do
    @moduledoc """
    The %Practice.BST.Node{} Struct
    """
    defstruct data: nil, left_child: nil, right_child: nil
  end

  @doc """
  Creates a new BST
  """
  def new(value) do
    %Practice.BST.Node{data: value}
  end

  def insert(value) do
  end
end
