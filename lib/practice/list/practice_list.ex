defmodule Practice.List do
  @moduledoc """
  Solutions for common coding problems found in interviews and/or coding challenges when working with lists
  """

  @doc """
  Sum -Given a list of integers, return the sum
   ## Example

      iex> Practice.List.sum([1, 2, 3, 4, 5])
      15
  """
  def sum(list) when is_list(list) do
    Enum.reduce(list, fn x, acc -> x + acc end)
  end

  @doc """
  TwoSum- Given a list of integers, find the first two integers that sum to the target number
    ## Example
      iex> Practice.List.two_sum([0, 2, 11, 19, 90], 21)
      [1, 3]
  """
  def two_sum(numbers, sum) do
    Enum.with_index(numbers)
    |> Enum.reduce_while([], fn {x, i}, acc ->
      y = sum - x

      case Enum.find_index(numbers, &(&1 == y)) do
        nil -> {:cont, acc}
        j -> {:halt, [i, j]}
      end
    end)
  end

  @doc """
  FindEquilibrium- Find the equilibrium index(es) from a given list
    ## Examples
    iex> Practice.List.find_equilibrium([-7, 1, 5, 2, -4, 3, 0])
    [3, 6]
    iex> Practice.List.find_equilibrium([2, 9, 2])
    [1]
    iex> Practice.List.find_equilibrium([7, 2, 3, 8, 0, 11, 1])
    [3]
  """
  def find_equilibrium(list) do
    last = length(list)

    Enum.filter(0..(last - 1), fn i ->
      Enum.sum(Enum.slice(list, 0, i)) == Enum.sum(Enum.slice(list, (i + 1)..last))
    end)
  end

  @doc """
  GCD - Greatest Common Denominator
  """
  def gcd(x, 0), do: x
  def gcd(x, y), do: gcd(y, rem(x, y))
end
