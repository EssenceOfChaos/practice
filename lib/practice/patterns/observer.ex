defmodule Observer do
  def start do
    spawn(__MODULE__, :listen, [])
  end

  def listen do
    receive do
      event ->
        IO.puts("Received #{event}")
    end

    listen
  end
end
