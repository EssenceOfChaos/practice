defmodule Practice.String do
  @moduledoc """
  Module for various functions involving strings
  """
  @doc """
  unique?/1 - check if a string has all unique characters.
    ## Examples
       iex> Practice.String.unique?("string")
       true
       iex> Practice.String.unique?("bragging")
       false
  """
  def unique?(str) when is_binary(str) do
    count_chars(String.graphemes(str), Map.new())
    |> print()
  end

  def count_chars([], map), do: map

  def count_chars([first | rest], map) do
    case Map.has_key?(map, first) do
      true ->
        map = Map.update!(map, first, fn val -> val + 1 end)
        count_chars(rest, map)

      false ->
        map = Map.put_new(map, first, 1)
        count_chars(rest, map)
    end
  end

  defp print(map) do
    Enum.all?(Map.values(map), fn x -> x == 1 end)
  end

  @doc """
  binary_digits_from_int - returns the binary form of an integer
  """
  def binary_digits_from_int(x) do
    Integer.to_string(x, 2)
  end

  @doc """
  prefix - Check if string starts with prefix
  """
  def prefix(string, prefix) do
    String.starts_with?(string, prefix)
  end
end
