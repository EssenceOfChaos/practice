defmodule ExPractice do
  @moduledoc """
  Various functions for common coding problems found in interviews and/or coding challenges
  """
  @doc """
  unique?/1 - check if a string has all unique characters.
  """
  def unique?(str) when is_binary(str) do
    count_chars(String.graphemes(str), Map.new())
    |> print()
  end

  def count_chars([], map), do: map

  def count_chars([first | rest], map) do
    case Map.has_key?(map, first) do
      true ->
        map = Map.update!(map, first, fn val -> val + 1 end)
        count_chars(rest, map)

      false ->
        map = Map.put_new(map, first, 1)
        count_chars(rest, map)
    end
  end

  defp print(map) do
    IO.inspect(Enum.all?(Map.values(map), fn x -> x == 1 end))
  end
end

## expect true
ExPractice.unique?("string")
## expect false
ExPractice.unique?("bragging")
