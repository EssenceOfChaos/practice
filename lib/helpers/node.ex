defmodule Node do
  @moduledoc """
  Definition of a Node
  """
  defstruct value: nil, left_child: nil, right_child: nil
end
