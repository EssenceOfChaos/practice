defmodule Lists do
  @moduledoc """
  Module for working with lists
  """
  @doc """
  Given a list of integers, return the sum
   ## Examples

      iex> Lists.sum([1, 2, 3, 4, 5])
      15
  """
  def sum(list) do
    IO.inspect(Enum.reduce(list, fn x, acc -> x + acc end))
  end

  @doc """
  Given a list of integers, find the first two integers that sum to the target number
  """
  def two_sum(numbers, sum) do
    Enum.with_index(numbers)
    |> Enum.reduce_while([], fn {x, i}, acc ->
      y = sum - x

      case Enum.find_index(numbers, &(&1 == y)) do
        nil -> {:cont, acc}
        j -> {:halt, [i, j]}
      end
    end)
  end

  @doc """
  Find the equilibrium index from a given list
  """
  def find_equilibrium(list) do
    last = length(list)

    Enum.filter(0..(last - 1), fn i ->
      Enum.sum(Enum.slice(list, 0, i)) == Enum.sum(Enum.slice(list, (i + 1)..last))
    end)
  end
end

# Sum- the list - expect 15
Lists.sum([1, 2, 3, 4, 5])

# Two Sum -expect [1, 3]
IO.inspect(Lists.two_sum([0, 2, 11, 19, 90], 21))
# Return the equilibrium index(es), otherwise return an empty list
indices = [
  [-7, 1, 5, 2, -4, 3, 0],
  [2, 4, 6],
  [7, 2, 3, 8, 0, 11, 1],
  [2, 9, 2],
  [1, -1, 1, -1, 1, -1, 1]
]

Enum.each(indices, fn list ->
  IO.puts("#{inspect(list)} => #{inspect(Lists.find_equilibrium(list))}")
end)

IO.inspect(MapSet.new([5, 2, 3, 8, 6, 0, 1, 9]))
