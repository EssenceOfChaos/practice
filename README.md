# Elixir Practice

### Created by Frederick John

This repository is a colleciton of solutions I've assembled over time from various interviews and programming challenges. The solutions are written in Elixir. The purpose of this repo is to serve as a learning opportunity and point of reference for other Elixir developers.

### Contributions welcome

### Please note

Although I'm attempting to adequately document and test the solutions, there may be some edge cases that are not accounted for. Also, some solutions may not be fully optimized. If you want to add more solutions or optimize existing ones, feel free to submit a pull request.
