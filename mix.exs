defmodule Practice.MixProject do
  use Mix.Project

  def project do
    [
      app: :practice,
      version: "0.1.0",
      elixir: "~> 1.6",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      # Docs
      name: "ElixirPractice",
      source_url: "https://bitbucket.org/EssenceOfChaos/practice/src/master/",
      homepage_url: "",
      # The main page in the docs
      docs: [main: "Practice", logo: "assets/images/elixir-logo.png", extras: ["README.md"]]
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger],
      mod: {Practice.Application, []}
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      {:ex_doc, "~> 0.16", only: :dev, runtime: false},
      {:earmark, "~> 1.2"},
      {:credo, "~> 0.9.1", only: [:dev, :test], runtime: false},
      {:benchfella, "~> 0.3.0"}
    ]
  end
end
